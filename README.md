### Omnilytics Data Challenge
* The solutions are done in Jupyter notebook `main.ipynb`.
* The same is exported out to `main.pdf` with all outputs.
* All the references for this project is included at the end of the notebook.

### Dependencies
```
numpy
pandas
sklearn
matplotlib
keras (with tensorlow backend)
```
